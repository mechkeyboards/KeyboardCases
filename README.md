# KeyboardCases
My different keyboard cases.

Layout designed with http://www.keyboard-layout-editor.com/, dxf generated with http://builder.swillkb.com/ and updated/modified with Qcad.

![example](https://galeries.darkou.fr/var/resizes/Informatique/Mech-Keyboards/Purple-Satan-%28Gateron-Brown---HHKB-layout---PCB-mounted%29/IMG_7499.JPG?m=1479239309 "Example")
